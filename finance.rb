require 'sinatra'
require 'rest-client'
require 'csv'

get '/' do
	"Hello! Look for stock information by adding the ticker directly to the URL! Example: stockinfo.cloudfoundry.com/VMW"
end

get '/*' do |tick|
	website = RestClient.get("http://download.finance.yahoo.com/d",
		{:params=> {:s=>tick, :f=>"nc1p2o"}})
	data= CSV.parse_line(website)
	@color = (data[1].start_with? '+') ? "green" : "red"
	@ticker=tick.upcase
	@name=data[0]
	@change=data[1]
	@percent=data[2]
	@open=data[3]
	erb :index
end

